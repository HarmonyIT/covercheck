package factory;

import managers.FileReaderManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserFactory {
    public static WebDriver getWebDriver(String browserName){
        WebDriver driver;
        if (browserName.equals("Chrome")){
            System.setProperty("webdriver.chrome.driver", FileReaderManager.getInstance().getConfigReader().getDriverPath() );
            driver = new ChromeDriver();
        }else{
            driver = new ChromeDriver();
        }
        return driver;
    }
}
