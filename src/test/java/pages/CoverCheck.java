package pages;

import managers.FileReaderManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CoverCheck {
    WebDriver driver;
    public CoverCheck(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "vehicleReg") WebElement vehicleRegElement;
    public void enterVehicleReg(String vehicleReg) {
        vehicleRegElement.sendKeys(vehicleReg);
    }

    @FindBy(name = "btnfind") WebElement findVehicleButtonElement;
    public void clickFindVehicleButton() {
        findVehicleButtonElement.click();
    }

    @FindBy(className = "result") WebElement resultElement;
    public void assertCoverage(String vehicleReg){
        Assert.assertTrue(resultElement.getText().contains(vehicleReg));
    }

    public void assertNoCoverage(){
        Assert.assertTrue(resultElement.getText().contains("Sorry record not found"));
    }

    public void assertCoverCheckPageTitle(String title){
        Assert.assertEquals(title, driver.getTitle());
    }

    public void getCoverCheckForm(String title){
        driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("vehicleReg")));
        assertCoverCheckPageTitle(title);
    }
}
