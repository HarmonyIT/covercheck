package managers;

import org.openqa.selenium.WebDriver;
import pages.CoverCheck;

public class PageObjectManager {
    private WebDriver driver;
    private CoverCheck coverCheckPage;

    public PageObjectManager(WebDriver driver){
        this.driver = driver;
    }

    public CoverCheck getCoverCheckPage(){
        return (coverCheckPage == null)? coverCheckPage = new CoverCheck(driver):coverCheckPage;
    }

}

