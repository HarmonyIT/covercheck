package dbConnectionPackage.factory;

import dbConnectionPackage.DbConnection;

public interface DbConnectionFactory {
    DbConnection getConnection();
}
