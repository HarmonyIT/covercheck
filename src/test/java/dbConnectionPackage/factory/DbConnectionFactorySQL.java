package dbConnectionPackage.factory;

import dbConnectionPackage.DbConnection;
import dbConnectionPackage.DbConnectionSQL;

public class DbConnectionFactorySQL implements DbConnectionFactory {
    @Override
    public DbConnection getConnection() {
        return new DbConnectionSQL();
    }
}
