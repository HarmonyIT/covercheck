package dbConnectionPackage.factory;

import dbConnectionPackage.DbConnection;
import dbConnectionPackage.DbConnectionMongo;

public class DbConnectionFactoryMongo implements DbConnectionFactory {
    @Override
    public DbConnection getConnection() {
        return new DbConnectionMongo();
    }
}
