package dbConnectionPackage;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
//import org.bson.Document;

public class DbConnectionMongo implements DbConnection {
    @Override
    public void connect() {
        System.out.println("Connecting to MongoDB...");
    }

    public MongoCollection getCollection(){
        MongoClient mongoClient = MongoClients.create();
        MongoDatabase database = mongoClient.getDatabase("vahreports");
//        MongoCollection<Document> collection = database.getCollection("test");
//        BasicDBObject document = new BasicDBObject();
//        document.put("name", "Shubham");
//        document.put("company", "Baeldung");
//        collection.insert(document);
        return database.getCollection("test");
    }
}
