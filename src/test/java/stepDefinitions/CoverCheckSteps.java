package stepDefinitions;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.CoverCheck;

public class CoverCheckSteps {
    TestContext testContext;
    CoverCheck coverCheckPage;

    public CoverCheckSteps(TestContext context) {
        testContext = context;
        coverCheckPage = testContext.getPageObjectManager().getCoverCheckPage();
    }

    @Given("^User Navigates to \"([^\"]*)\"$")
    public void user_Navigates_to(String title) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        coverCheckPage.getCoverCheckForm(title);
    }

    @When("^submits \"([^\"]*)\"$")
    public void submits(String vehicleReg) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        coverCheckPage.enterVehicleReg(vehicleReg);
        coverCheckPage.clickFindVehicleButton();
    }

    @Then("^Results show \"([^\"]*)\" coverage$")
    public void results_show_coverage(String vehicleReg) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        coverCheckPage.assertCoverage(vehicleReg);
    }

    @Then("^Results show no coverage$")
    public void results_show_no_coverage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        coverCheckPage.assertNoCoverage();
    }

}
