package dbWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dbConnectionPackage.DbConnection;
import dbConnectionPackage.factory.DbConnectionFactory;
import dbConnectionPackage.factory.DbConnectionFactoryMongo;
import dbConnectionPackage.factory.DbConnectionFactorySQL;
import enums.DatabaseType;
import org.bson.Document;

public class DbWriter {
    private final DbConnectionFactory dbConnectionFactory;
    private final DbConnection connection;
    private final MongoCollection table;

    public DbWriter(){
        DatabaseType databaseType = DatabaseType.MONGODB;
        DbConnectionFactory dbConnectionFactory = getConnectionFactory(databaseType);
        this.dbConnectionFactory = dbConnectionFactory;
        this.connection = dbConnectionFactory.getConnection();
        MongoClient mongoClient = MongoClients.create();
        MongoDatabase database = mongoClient.getDatabase("vahreports");
        this.table = database.getCollection("test");
    }

    private static DbConnectionFactory getConnectionFactory(DatabaseType databaseType){
        switch(databaseType){
            case MYSQL: return new DbConnectionFactorySQL();
            default: return new DbConnectionFactoryMongo();
        }
    }

    public void log(String feature, String scenario, String step){
//        BasicDBObject document = new BasicDBObject();
        String[] words = feature.split(";");
        Document document = new Document();
        document.put("feature", words[0]);
        document.put("scenario", scenario);
        document.put("step", step);
        table.insertOne(document);
    }

//    public void start(){
//        connection.connect();
//    }
}
