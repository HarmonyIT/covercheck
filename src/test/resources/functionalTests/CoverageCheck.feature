@complete
Feature: Coverage Check feature
  Verify if a Vehicle is Registered with us
  For insurance Coverage

  Scenario Outline: Check valid registration
    Given User Navigates to "Dealer Portal"
    When submits "<registration>"
    Then Results show "<registration>" coverage

    Examples:
      | registration |
      | OV12UYY |

  Scenario Outline: Check invalid registration
    Given User Navigates to "Dealer Portal"
    When submits "<registration>"
    Then Results show no coverage

    Examples:
      | registration |
      | OV12UYX |

